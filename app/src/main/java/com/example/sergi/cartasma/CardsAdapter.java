package com.example.sergi.cartasma;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.databinding.DataBindingUtil;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.sergi.cartasma.databinding.LayoutListaBinding;

import java.util.List;

/**
 * Created by Sergi on 12/12/2017.
 */

public class CardsAdapter extends ArrayAdapter {
    public CardsAdapter(Context context, int resource, List<Card> objects) {
        super(context, resource, objects);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Card card = (Card) getItem(position);
        Log.w("XXXX", card.toString());

        LayoutListaBinding binding = null;



        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            binding = DataBindingUtil.inflate(inflater, R.layout.layout_lista,parent,false);
        } else {
            binding = DataBindingUtil.getBinding(convertView);
        }



        binding.tvCard.setText(card.getNombre());
        Glide.with(getContext()).load(
                card.getImageUrl()
        ).into(binding.ivImageView);

        return binding.getRoot();


    }

}
