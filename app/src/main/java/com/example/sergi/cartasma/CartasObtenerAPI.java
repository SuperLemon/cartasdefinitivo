package com.example.sergi.cartasma;

import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Sergi on 16/11/2017.
 */

public class CartasObtenerAPI {
    private final String BASE_URL = "https://api.magicthegathering.io/v1";

    ArrayList<Card> getCards() {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("cards")
                .build();
        String url = builtUri.toString();

        try {
            String JsonResponse = HttpUtils.get(url);
            return proccesJson(JsonResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Card> proccesJson(String jsonResponse) {
        ArrayList<Card> cards =new ArrayList<>();
        try {
            JSONObject data = new JSONObject(jsonResponse);
            JSONArray jsonCards = data.getJSONArray("cards");
            for (int i = 0; i<jsonCards.length(); i++) {
                JSONObject jsonCard = jsonCards.getJSONObject(i);


                Card card = new Card();
                card.setNombre(jsonCard.getString("name"));
                card.setImageUrl(jsonCard.getString("imageUrl"));
                card.setDescripcion(jsonCard.getString("text"));

                cards.add(card);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cards;
    }
}
