package com.example.sergi.cartasma;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by petla on 09/01/2018.
 */

public class CardsViewModel extends AndroidViewModel {
    private final Application app;
    private final AppDatabase appDatabase;
    private final CardDao cardDao;
    private static final int PAGES = 10;

    public CardsViewModel(Application application, AppDatabase appDatabase, CardDao cardDao) {
        super(application);

        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(this.getApplication());
        this.cardDao = appDatabase.getCardDao();
    }

    public LiveData<List<Card>> getCards() {
        return cardDao.getCards();
    }

    public void reload() {
        RefreshDataTask task = new RefreshDataTask();
        task.execute();

    }
    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Card>> {
        @Override
        protected ArrayList<Card> doInBackground(Void... voids) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(
                    app.getApplicationContext()
            );
            CartasObtenerAPI api = new CartasObtenerAPI();
            ArrayList<Card> result = api.getCards();

            Log.d("DEBUG", result.toString());

            cardDao.deleteCards();
            cardDao.addCards(result);

            return result;
        }

    }
}
