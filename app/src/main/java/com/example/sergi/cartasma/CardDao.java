package com.example.sergi.cartasma;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by petla on 09/01/2018.
 */
@Dao
public interface CardDao {
    @Query("select * from card")
    LiveData<List<Card>> getCards();

    @Insert
    void addCard(Card card);

    @Insert
    void addCards(List<Card> cards);

    @Delete
    void deleteCard(Card card);

    @Query("DELETE FROM card")
    void deleteCards();
}
